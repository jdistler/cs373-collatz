#!/usr/bin/env python3
"""
By Joseph Distler
"""
# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    """
    Test harness for Collatz
    """
    # ----
    # read
    # ----

    def test_read(self):
        """
        Tests whether collatz can parse a string
        """
        str_input = "1 10\n"
        i, j = collatz_read(str_input)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(1, 10)
        self.assertEqual(result_max, 20)

    def test_eval_2(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(100, 200)
        self.assertEqual(result_max, 125)

    def test_eval_3(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(201, 210)
        self.assertEqual(result_max, 89)

    def test_eval_4(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(900, 1000)
        self.assertEqual(result_max, 174)

    def test_eval_5(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(200, 400)
        self.assertEqual(result_max, 144)

    def test_eval_6(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(200, 501)
        self.assertEqual(result_max, 144)

    def test_eval_7(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(200, 871)
        self.assertEqual(result_max, 179)

    def test_eval_8(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(200, 872)
        self.assertEqual(result_max, 179)

    def test_eval_9(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(400, 872)
        self.assertEqual(result_max, 179)

    def test_eval_10(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(200, 2000)
        self.assertEqual(result_max, 182)

    def test_eval_11(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(2000, 200)
        self.assertEqual(result_max, 182)

    def test_eval_12(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(1, 10000)
        self.assertEqual(result_max, 262)

    def test_eval_13(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(1, 1)
        self.assertEqual(result_max, 1)

    def test_eval_14(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(1, 2)
        self.assertEqual(result_max, 2)

    def test_eval_15(self):
        """
        Tests max cycle for given range
        """
        result_max = collatz_eval(1, 3)
        self.assertEqual(result_max, 8)

    # -----
    # print
    # -----

    def test_print(self):
        """
        Tests the IO result of running print
        """
        writer = StringIO()
        collatz_print(writer, 1, 10, 20)
        self.assertEqual(writer.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """
        Tests the IO result of running solve with a set string
        """
        reader = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        writer = StringIO()
        collatz_solve(reader, writer)
        self.assertEqual(
            writer.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()
