#!/usr/bin/env python3
"""
By Joseph Distler
"""
# -------------
# RunCollatz.py
# -------------

# -------
# imports
# -------

from sys import stdin, stdout
from Collatz import collatz_solve

# ----
# main
# ----

if __name__ == "__main__":
    collatz_solve(stdin, stdout)
